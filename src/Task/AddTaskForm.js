import React from 'react';
import './task.css';

const AddTaskFrom = (props) => {
    return (
        <div className="form">
            <input type="text" placeholder="Add new task" onChange={props.change} />
            <input type="button" value="Add" onClick={props.add} />
        </div>
    );
}
export default AddTaskFrom;
