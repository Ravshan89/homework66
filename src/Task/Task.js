import React from 'react';
import './task.css';

const Task = (props) => {
    return (
        <div className="task">
            <p>{props.description} <input type="button" value="Delete" onClick={props.remove} /></p>    
        </div>
    );
}
export default Task;
