import React, { Component } from 'react';
import AddTaskForm from './Task/AddTaskForm';
import Task from './Task/Task';
import './App.css'


class App extends Component {
    state = {
        currentTask: '',
        task: [
            { description: 'Buy milk', id: 1 },
            { description: 'Walk with dog', id: 2 },
            { description: 'Do homework', id: 3 }
        ]
    };

    AddTaskDescription = (event) => {
        const currentTask = event.target.value;
        this.setState({ currentTask });

    }
    AddTask = () => {
        const task = [...this.state.task];
        task.sort(function (a, b) {
            if (a.id > b.id) {
                return 1;
            }
            if (a.id < b.id) {
                return -1;
            }
            return 0;
        });
        task.unshift({
            description: this.state.currentTask,
            id: task.length === 0 ? 0 : task[task.length - 1].id + 1
        });
        this.setState({ task }, function () {
            return console.log(this.state.task)
        });

    }
    removeTask = (id) => {
        const index = this.state.task.findIndex(t => t.id === id);
        const task = [...this.state.task];
        task.splice(index, 1);
        this.setState({ task });
    }
    render() {
        return (
            <div className="container">
                <AddTaskForm change={event => this.AddTaskDescription(event)} add={() => this.AddTask(this.state.task.length)} />
                {
                    this.state.task.map((task) => {
                        return <Task description={task.description} key={task.id} remove={() => this.removeTask(task.id)}></Task>
                    }
                    )}
            </div>
        );
    }
}

export default App;
